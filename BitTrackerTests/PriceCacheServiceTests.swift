//
//  PriceCacheServiceTests.swift
//  BitTrackerTests
//
//  Created by Ian Alexander Rahman on 2/3/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import XCTest
@testable import BitTracker

class PriceCacheServiceTests: XCTestCase {

    // MARK: - Properties

    var cache: PriceCacheService?

    // MARK: - Functions

    override func setUp() {
        cache = PriceCacheService(suiteName: String(describing: self))
        cache?.reset()
    }

    override func tearDown() {
        cache = nil
    }

    func testCacheCurrentPrice() throws {
        XCTAssertNil(try cache?.loadCachedCurrentPrice(), "Cache failed to reset")

        guard let price = try? TestHelpers.makeBitcoinPrice() else {
            XCTFail("Failed to create current price from data")
            return
        }

        try cache?.save(currentPrice: price)

        guard let loadedPrice = try cache?.loadCachedCurrentPrice() else {
            XCTFail("Failed to load current price from cache")
            return
        }

        XCTAssertEqual(price, loadedPrice, "Cached bitcoin price is not a match")
    }

    func testCacheHistoricalPrices() throws {
        XCTAssertNil(try cache?.loadCachedHistoricalPrices(), "Cache failed to reset")

        guard let prices = try? TestHelpers.makeHistoricalPrices() else {
            XCTFail("Failed to create historical prices from data")
            return
        }

        try cache?.save(historicalPrices: prices)

        guard let loadedPrices = try cache?.loadCachedHistoricalPrices() else {
            XCTFail("Failed to load historical prices from cache")
            return
        }

        XCTAssertEqual(prices, loadedPrices, "Cached historical prices are not a match")
    }

}
