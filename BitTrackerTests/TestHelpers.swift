//
//  TestHelpers.swift
//  BitTrackerTests
//
//  Created by Ian Alexander Rahman on 2/3/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import Foundation
@testable import BitTracker

class TestHelpers {

    static let currentDataMock: Data = {
        return Data("""
{"time":{"updated":"Feb 3, 2019 21:22:00 UTC","updatedISO":"2019-02-03T21:22:00+00:00","updateduk":"Feb 3, 2019 at 21:22 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","chartName":"Bitcoin","bpi":{"USD":{"code":"USD","symbol":"&#36;","rate":"3,420.1150","description":"United States Dollar","rate_float":3420.115},"GBP":{"code":"GBP","symbol":"&pound;","rate":"2,615.7724","description":"British Pound Sterling","rate_float":2615.7724},"EUR":{"code":"EUR","symbol":"&euro;","rate":"2,987.3336","description":"Euro","rate_float":2987.3336}}}
""".utf8)
    }()

    static let historicalDataMock: Data = {
        return Data("""
{"bpi":{"2019-01-03":3363.3866,"2019-01-04":3390.4831,"2019-01-05":3365.8718,"2019-01-06":3580.0374,"2019-01-07":3521.6176,"2019-01-08":3522.3739,"2019-01-09":3487.9205,"2019-01-10":3191.4506,"2019-01-11":3192.74,"2019-01-12":3189.9051,"2019-01-13":3087.8181,"2019-01-14":3231.7379,"2019-01-15":3159.7797,"2019-01-16":3185.7644,"2019-01-17":3218.5745,"2019-01-18":3194.0016,"2019-01-19":3262.775,"2019-01-20":3125.903,"2019-01-21":3121.0729,"2019-01-22":3166.7919,"2019-01-23":3142.1863,"2019-01-24":3183.6896,"2019-01-25":3141.593,"2019-01-26":3139.2404,"2019-01-27":3114.1167,"2019-01-28":3022.7909,"2019-01-29":2991.7181,"2019-01-30":3032.7037,"2019-01-31":3007.0764,"2019-02-01":3027.0589,"2019-02-02":3025.2834},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index. BPI value data returned as EUR.","time":{"updated":"Feb 3, 2019 00:03:00 UTC","updatedISO":"2019-02-03T00:03:00+00:00"}}
""".utf8)
    }()

    static func makeBitcoinPrice() throws -> BitcoinPrice {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return try decoder.decode(BitcoinPrice.self, from: currentDataMock)
    }

    static func makeHistoricalPrices() throws -> HistoricalPrices {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return try decoder.decode(HistoricalPrices.self, from: historicalDataMock)
    }

}
