//
//  PriceListViewModelTests.swift
//  BitTrackerTests
//
//  Created by Ian Alexander Rahman on 2/3/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import XCTest
@testable import BitTracker

class PriceListViewModelTests: XCTestCase {

    // MARK: - Properties

    private let model = PriceListViewModel()

    // MARK: - Functions

    override func setUp() {

    }

    override func tearDown() {
        model.handleCurrentPriceUpdate = nil
        model.handleHistoricalPricesUpdate = nil
        model.handleSelectedCurrencyUpdate = nil
        model.historicalPrices = nil
        model.currentPrice = nil
    }

    func testHandleCurrentPriceUpdate() throws {
        XCTAssertNil(model.handleCurrentPriceUpdate, "Handle current price update is not initially nil")

        let testPrice = try TestHelpers.makeBitcoinPrice()
        let handlerExpectation = expectation(description: "Completed")

        model.handleCurrentPriceUpdate = { price in
            XCTAssert(Thread.isMainThread, "Handle current price update is not being run on the main thread")
            XCTAssertEqual(price, testPrice, "Current price does not match test price")
            handlerExpectation.fulfill()
        }

        model.currentPrice = testPrice

        XCTAssertNotNil(model.currentPrice, "Current price failed to set")
        waitForExpectations(timeout: 0.3, handler: nil)
    }

    func testHandleHistoricalPricesUpdate() throws {
        XCTAssertNil(model.handleHistoricalPricesUpdate, "Handle current price update is not initially nil")

        let testPrices = try TestHelpers.makeHistoricalPrices()
        let handlerExpectation = expectation(description: "Completed")

        model.handleHistoricalPricesUpdate = { prices in
            XCTAssert(Thread.isMainThread, "Handle historical prices update is not being run on the main thread")
            XCTAssertEqual(prices, testPrices, "Historical prices do not match test prices")
            handlerExpectation.fulfill()
        }

        model.historicalPrices = testPrices

        XCTAssertNotNil(model.historicalPrices, "Historical prices failed to set")
        waitForExpectations(timeout: 0.3, handler: nil)
    }

    func testHandleSelectedCurrencyUpdate() {
        XCTAssertNil(model.handleSelectedCurrencyUpdate, "Handle seleclted currency update is not initially nil")

        let handlerExpectation = expectation(description: "Completed")

        model.handleSelectedCurrencyUpdate = { [weak self] in
            XCTAssertEqual(Currency.usd, self?.model.selectedCurrency, "Selected currency does not match test currency")
            handlerExpectation.fulfill()
        }

        model.selectedCurrency = .usd
        waitForExpectations(timeout: 0.3, handler: nil)
    }
    
}
