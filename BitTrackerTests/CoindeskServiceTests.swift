//
//  CoindeskServiceTests.swift
//  BitTrackerTests
//
//  Created by Ian Alexander Rahman on 2/3/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import XCTest
@testable import BitTracker

class CoindeskServiceTests: XCTestCase {

    // MARK: - Mock Classes

    class URLProtocolMock: URLProtocol {
        static var testURLs: [URL?: Data] = [:]

        override class func canInit(with request: URLRequest) -> Bool {
            return true
        }

        override class func canonicalRequest(for request: URLRequest) -> URLRequest {
            return request
        }

        override func startLoading() {
            if let url = request.url, let data = URLProtocolMock.testURLs[url] {
                self.client?.urlProtocol(self, didLoad: data)
            }

            self.client?.urlProtocolDidFinishLoading(self)
        }

        override func stopLoading() { }
    }

    // MARK: - Properties

    var coindesk: CoindeskService?

    // MARK: - Functions

    override func setUp() {
        let current = URL(string: "https://api.coindesk.com/v1/bpi/currentprice.json")
        let historical = URL(string: "https://api.coindesk.com/v1/bpi/historical/close.json?currency=EUR")
        URLProtocolMock.testURLs = [current: TestHelpers.currentDataMock, historical: TestHelpers.historicalDataMock]
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let session = URLSession(configuration: config)
        coindesk = CoindeskService(session: session)
    }

    override func tearDown() {
        coindesk = nil
    }

    func testFetchCurrentPrice() {
        let handlerExpectation = expectation(description: "Completed")

        coindesk?.fetch(.currentPrice) { result in
            guard let price = try? result.decoded() as BitcoinPrice else {
                XCTFail("Failed to create Bitcoin Price from data")
                return
            }

            XCTAssert(price.bpi.count > 0, "Current Price response does not contain expected data")
            // FIXME: Tests should also check to ensure data structure and contents are as we expect.

            handlerExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 0.3, handler: nil)
    }

    func testFetchHistoricalPrices() {
        let handlerExpectation = expectation(description: "Completed")

        coindesk?.fetch(.historicalPrices(currency: .eur)) { result in
            guard let prices = try? result.decoded() as HistoricalPrices else {
                XCTFail("Failed to create Historical Prices from data")
                return
            }

            XCTAssert(prices.bpi.count > 0, "Historical Prices response does not contain expected data")
            // FIXME: Tests should also check to ensure data structure and contents are as we expect.

            handlerExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.3, handler: nil)
    }

}
