//
//  InterfaceController.swift
//  BitTracker Watch App Extension
//
//  Created by Ian Alexander Rahman on 1/26/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    // MARK: - Outlets

    @IBOutlet weak var currentPriceLabel: WKInterfaceLabel!
    @IBOutlet weak var table: WKInterfaceTable!

    // MARK: - Properties

    private let coindesk = CoindeskService()
    // TODO: Allow users to select (and save) a currency on the watch
    private let selectedCurrency: Currency = .eur

    // MARK: - Lifecycle

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        super.willActivate()
        refreshData()
    }

    // TODO: Rely on cached data instead of making a network call at each activation; push from iPhone.
    // Use a Watch Connectivity Session to pass information from the phone to the watch.
    // https://developer.apple.com/documentation/watchconnectivity/wcsession
    private func refreshData() {
        updateCurrentPrice()
        updateHistoricalPrices()
    }

    private func updateCurrentPrice() {
        coindesk.fetch(.currentPrice) { [weak self] result in
            do {
                let currentPrice = try result.decoded() as BitcoinPrice
                if let priceData = currentPrice.bpi["EUR"],
                    let selectedCurrency = self?.selectedCurrency {
                    DispatchQueue.main.async {
                        self?.currentPriceLabel.setText(String(format: "%@%.2f",
                                                               selectedCurrency.symbol,
                                                               priceData.rate_float))
                    }
                }
            } catch {
                // TODO: Handle error
                print("Error: \(error.localizedDescription)")
            }
        }
    }

    private func updateHistoricalPrices() {
        coindesk.fetch(.historicalPrices(currency: selectedCurrency)) { [weak self] result in
            do {
                let prices = try result.decoded() as HistoricalPrices
                var orderedPrices: [(date: String, price: Double)] = []

                prices.bpi.keys.sorted().reversed().forEach {
                    if let value = prices.bpi[$0] {
                        orderedPrices.append(($0, value))
                    }
                }

                self?.table.setNumberOfRows(orderedPrices.count, withRowType: "PriceRow")

                DispatchQueue.main.async {
                    for i in 0..<orderedPrices.count {
                        let controller = self?.table.rowController(at: i)
                        let price = orderedPrices[i]
                        if let controller = controller as? PriceRowController,
                            let selectedCurrency = self?.selectedCurrency {
                            controller.priceLabel.setText(String(format: "%@%.2f",
                                                                 selectedCurrency.symbol,
                                                                 price.price))
                            controller.dateLabel.setText("\(price.date)")
                        }
                    }
                }
            } catch {
                // TODO: Handle error
                print("Error: \(error.localizedDescription)")
            }
        }
    }

}
