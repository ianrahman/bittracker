//
//  PriceRowController.swift
//  BitTracker Watch App Extension
//
//  Created by Ian Alexander Rahman on 2/1/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import WatchKit

class PriceRowController: NSObject {

    @IBOutlet weak var priceLabel: WKInterfaceLabel!
    @IBOutlet weak var dateLabel: WKInterfaceLabel!
}
