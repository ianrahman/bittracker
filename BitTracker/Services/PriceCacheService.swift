//
//  PriceCacheService.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/27/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import Foundation

enum CacheError: Error {
    case decodeError, encodeError
}

/**
 Manages the persistent storage of current and historical bitcoin prices.

 Under most circumstances, User Defaults is preferable only when storing very small amounts of data, and then
 usually only if the data relates to the preferences and settings of the user which pertain to a particular
 installation of the app. When storing significant amounts of data in models with graphs more complex than just
 a property or two, or when storing large numbers of items, Core Data is the preferred choice.

 In practice, the network calls made by this sample app result in payloads under 2 KB in size with very simple
 structures that are able to be stored quickly and easily with User Defaults. Furthermore, we store only the
 most recent records and effectively stay well below the limits of User Defaults.
 */
final class PriceCacheService {

    // MARK: - Properties

    private let defaults: UserDefaults?
    private let currentPriceKey = "currentPrice"
    private let historicalPricesKey = "historicalPrices"

    // MARK: - Lifecycle

    init(suiteName: String? = nil) {
        self.defaults = UserDefaults(suiteName: suiteName)
    }

    // MARK: - Functions

    func loadCachedCurrentPrice() throws -> BitcoinPrice? {
        if let data = defaults?.data(forKey: currentPriceKey) {
            do {
                let decoder = JSONDecoder()
                return try decoder.decode(BitcoinPrice.self, from: data)
            } catch {
                throw CacheError.decodeError
            }
        } else {
            return nil
        }
    }

    func loadCachedHistoricalPrices() throws -> HistoricalPrices? {
        if let data = defaults?.data(forKey: historicalPricesKey) {
            do {
                let decoder = JSONDecoder()
                return try decoder.decode(HistoricalPrices.self, from: data)
            } catch {
                throw CacheError.decodeError
            }
        } else {
            return nil
        }
    }

    func save(currentPrice value: BitcoinPrice) throws {
        let encoded = try JSONEncoder().encode(value)
        defaults?.set(encoded, forKey: currentPriceKey)
    }

    func save(historicalPrices value: HistoricalPrices) throws {
        let encoded = try JSONEncoder().encode(value)
        defaults?.set(encoded, forKey: historicalPricesKey)
    }

    func reset() {
        defaults?.removeObject(forKey: currentPriceKey)
        defaults?.removeObject(forKey: historicalPricesKey)
    }
}
