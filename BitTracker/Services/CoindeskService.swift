//
//  CoindeskService.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/26/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import Foundation

enum CoindeskResource {
    case currentPrice
    case historicalPrices(currency: Currency)
}

enum CoindeskError: Error {
    case networkError, invalidURL, dataUnavailable
}

final class CoindeskService {

    typealias Handler = (Result<Data, CoindeskError>) -> Void

    // MARK: - Static Properties

    private static let defaultSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        // We ignore the local cache data to prevent a trivial warning when loading from the watch:
        // https://stackoverflow.com/questions/52404748/networking-with-alamofire-on-watchos
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        let session = URLSession(configuration: configuration)
        return session
    }()

    // MARK: - Instance Properties

    private let session: URLSession
    private let scheme = "https"
    private let host = "api.coindesk.com"

    // MARK: - Lifecycle

    init(session: URLSession = defaultSession) {
        self.session = session
    }

    // MARK: - Functions

    func fetch(_ resource: CoindeskResource, then handle: @escaping Handler) {
        let components: URLComponents
        switch resource {
        case .currentPrice:
            components = makeCurrentPriceComponents()
        case .historicalPrices(let currency):
            components = makeHistoricalPriceComponents(for: currency)
        }

        guard let url = components.url else {
            handle(.failure(.invalidURL))
            return
        }

        session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                handle(.failure(.networkError))
            } else if let data = data {
                handle(.success(data))
            } else {
                handle(.failure(.dataUnavailable))
            }
        }.resume()
    }

}

// MARK: - Component Factories

extension CoindeskService {

    private func makeCurrentPriceComponents() -> URLComponents {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.path = "/v1/bpi/currentprice.json"
        return components
    }

    private func makeHistoricalPriceComponents(for currency: Currency) -> URLComponents {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.path = "/v1/bpi/historical/close.json"
        let currency = URLQueryItem(name: "currency", value: currency.iso4217)
        components.queryItems = [currency]
        return components
    }

}
