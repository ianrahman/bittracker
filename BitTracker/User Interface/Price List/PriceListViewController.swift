//
//  PriceListViewController.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/26/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import UIKit

class PriceListViewController: UIViewController {

    // MARK: Outlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentPriceLabel: UILabel!

    // MARK: - Properties

    private let model = PriceListViewModel()
    private let cache = PriceCacheService()
    private let coindesk = CoindeskService()
    private var timer: Timer?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        performSetup()
        loadCachedData()
    }

    override func viewWillAppear(_ animated: Bool) {
        timer = Timer.scheduledTimer(timeInterval: 60, target: self,
                                     selector: #selector(refreshData),
                                     userInfo: nil, repeats: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }

    // MARK: - General Functions

    private func performSetup() {
        tableView.dataSource = self
        tableView.delegate = self
        observeViewModel()
        refreshData()
    }

    private func observeViewModel() {
        model.handleCurrentPriceUpdate = { [weak self] bitcoinPrice in
            guard
                let currency = self?.model.selectedCurrency.iso4217,
                let priceData = self?.model.currentPrice?.bpi[currency]
                else {
                    self?.handleError(CoindeskError.dataUnavailable)
                    return
            }
            self?.currentPriceLabel.text = String(format: "%@%.2f",
                                                  self?.model.selectedCurrency.symbol ?? "",
                                                  priceData.rate_float)
        }
        model.handleHistoricalPricesUpdate = { [weak self] historicalPrices in
            self?.tableView.reloadData()
        }
        model.handleSelectedCurrencyUpdate = { [weak self] in
            self?.refreshData()
        }
    }

    private func loadCachedData() {
        DispatchQueue.main.async { [weak self] in
            do {
                self?.model.currentPrice = try self?.cache.loadCachedCurrentPrice()
                self?.model.historicalPrices = try self?.cache.loadCachedHistoricalPrices()
            } catch {
                self?.handleError(error)
            }
        }
    }

    @objc private func refreshData() {
        updateCurrentPrice()
        updateHistoricalPrices()
    }

    private func updateCurrentPrice() {
        coindesk.fetch(.currentPrice) { [weak self] result in
            do {
                let price = try result.decoded() as BitcoinPrice
                try self?.cache.save(currentPrice: price)
                self?.model.currentPrice = try self?.cache.loadCachedCurrentPrice()
            } catch {
                self?.handleError(error)
            }
        }
    }

    private func updateHistoricalPrices() {
        coindesk.fetch(.historicalPrices(currency: model.selectedCurrency)) { [weak self] result in
            do {
                let prices = try result.decoded() as HistoricalPrices
                try self?.cache.save(historicalPrices: prices)
                self?.model.historicalPrices = try self?.cache.loadCachedHistoricalPrices()
            } catch {
                self?.handleError(error)
            }
        }
    }

    // MARK: Actions

    @IBAction func handleCurrencyButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        for currency in Currency.allCases {
            let action = UIAlertAction(title: currency.iso4217,
                                       style: .default) { [unowned self] _ in
                                        self.model.selectedCurrency = currency
            }
            alert.addAction(action)
        }

        present(alert, animated: true)
    }

}

// MARK: - Error Handling

extension PriceListViewController {

    private func handleError(_ error: Error) {
        print("Error: \(error.localizedDescription)")
        
        if let cacheError = error as? CacheError {
            handle(cacheError: cacheError)
        } else if let coindeskError = error as? CoindeskError {
            handle(coindeskError: coindeskError)
        } else {
            // TODO: Handle other errors
        }
    }

    private func handle(cacheError: CacheError) {
        switch cacheError {
        case .decodeError:
            // TODO: Handle decode errors
            break
        case .encodeError:
            // TODO: Handle encode errors
            break
        }
    }

    private func handle(coindeskError: CoindeskError) {
        switch coindeskError {
        case .dataUnavailable:
            // TODO: Handle data missing errors
            break
        case .invalidURL:
            // TODO: Handle invalid URL errors
            break
        case .networkError:
            // TODO: Handle network errors
            break
        }
    }

}

// MARK: - Table View Data Source

extension PriceListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.orderedPrices.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PriceCell.reuseIdentifier,
                                                       for: indexPath) as? PriceCell else {
                                                        return UITableViewCell(frame: .zero)
        }

        let price = model.orderedPrices[indexPath.row]

        cell.priceLabel.text = String(format: "%@%.2f", model.selectedCurrency.symbol, price.price)
        cell.dateLabel.text = price.date

        return cell
    }

}

// MARK: - Table View Delegate

extension PriceListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PriceCell.cellHeight
    }

}
