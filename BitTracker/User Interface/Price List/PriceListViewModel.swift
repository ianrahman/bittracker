//
//  PriceListViewModel.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/26/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import Foundation

final class PriceListViewModel {

    // MARK: - Properties

    /// Called on the main thread.
    var handleCurrentPriceUpdate: ((BitcoinPrice?) -> Void)?
    /// Called on the main thread.
    var handleHistoricalPricesUpdate: ((HistoricalPrices?) -> Void)?
    var handleSelectedCurrencyUpdate: (() -> Void)?
    var currentPrice: BitcoinPrice? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.handleCurrentPriceUpdate?(self?.currentPrice)
            }
        }
    }
    var historicalPrices: HistoricalPrices? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.orderedPrices.removeAll()

                if let historicalPrices = self?.historicalPrices {
                    historicalPrices.bpi.keys.sorted().reversed().forEach {
                        if let value = historicalPrices.bpi[$0] {
                            self?.orderedPrices.append(($0, value))
                        }
                    }
                }
                self?.handleHistoricalPricesUpdate?(self?.historicalPrices)
            }
        }
    }
    var orderedPrices: [(date: String, price: Double)] = [] 
    var selectedCurrency: Currency = .eur {
        didSet {
            handleSelectedCurrencyUpdate?()
        }
    }

}
