//
//  PriceCell.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/26/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import UIKit

final class PriceCell: UITableViewCell {

    // MARK: - Static Properties

    static let cellHeight: CGFloat = 80
    static let reuseIdentifier = "PriceCell"

    // MARK: - Outlets

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

}
