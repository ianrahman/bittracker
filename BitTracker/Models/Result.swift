//
//  Result.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/26/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import Foundation

enum Result<Value, Error: Swift.Error> {
    case success(Value)
    case failure(Error)
}

extension Result {
    func resolve() throws -> Value {
        switch self {
        case .success(let value):
            return value
        case .failure(let error):
            throw error
        }
    }
}

extension Result where Value == Data {
    func decoded<T: Decodable>() throws -> T {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        let data = try resolve()
        return try decoder.decode(T.self, from: data)
    }
}
