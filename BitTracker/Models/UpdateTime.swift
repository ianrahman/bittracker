//
//  UpdateTime.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/27/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import Foundation

struct UpdateTime: Codable {
    var updated: String
    var updatedISO: Date
}

extension UpdateTime: Equatable { }
