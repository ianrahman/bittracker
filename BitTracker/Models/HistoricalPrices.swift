//
//  HistoricalPrices.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/26/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import Foundation

struct HistoricalPrices: Codable {
    var bpi: [String: Double]
    var time: UpdateTime
}

extension HistoricalPrices: Equatable {

    static func == (lhs: HistoricalPrices, rhs: HistoricalPrices) -> Bool {
        var isEqual = lhs.time == rhs.time && lhs.bpi.count == rhs.bpi.count

        for price in lhs.bpi {
            if rhs.bpi[price.key] == nil || rhs.bpi[price.key] != price.value {
                isEqual = false
            }
        }

        return isEqual
    }

}

/*
{
    "bpi":{
        "2013-09-01":128.2597,
        "2013-09-02":127.3648,
        "2013-09-03":127.5915,
        "2013-09-04":120.5738,
        "2013-09-05":120.5333
    },
    "disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index. BPI value data returned as USD.",
    "time":{
        "updated":"Sep 6, 2013 00:03:00 UTC",
        "updatedISO":"2013-09-06T00:03:00+00:00"
    }
}
 */
