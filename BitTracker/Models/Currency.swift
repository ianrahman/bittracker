//
//  Currency.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/26/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import Foundation

enum Currency: String, CaseIterable {
    case eur, gbp, usd

    var iso4217: String {
        return rawValue.uppercased()
    }

    // Can also be retrieved from the JSON payload under `symbol` via a Number Formatter.
    var symbol: String {
        switch self {
        case .eur:
            return "€"
        case .gbp:
            return "£"
        case .usd:
            return "$"
        }
    }
}
