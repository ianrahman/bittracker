//
//  BitcoinPrice.swift
//  BitTracker
//
//  Created by Ian Alexander Rahman on 1/26/19.
//  Copyright © 2019 Evergreen Labs. All rights reserved.
//

import Foundation

struct BitcoinPrice: Codable {
    struct BPIData: Codable, Equatable {
        let code: String
        let description: String
        let symbol: String
        let rate_float: Double

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            code = try values.decode(String.self, forKey: .code)
            description = try values.decode(String.self, forKey: .description)
            symbol = try values.decode(String.self, forKey: .symbol)
            rate_float = try values.decode(Double.self, forKey: .rate_float)
        }
    }

    let time: UpdateTime
    let bpi: [String: BPIData]

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        time = try values.decode(UpdateTime.self, forKey: .time)
        bpi = try values.decode([String: BPIData].self, forKey: .bpi)
    }
}

extension BitcoinPrice: Equatable {

    static func == (lhs: BitcoinPrice, rhs: BitcoinPrice) -> Bool {
        return lhs.time.updatedISO == rhs.time.updatedISO && lhs.bpi == rhs.bpi
    }

}

/*
{
    "time":{
        "updated":"Sep 18, 2013 17:27:00 UTC",
        "updatedISO":"2013-09-18T17:27:00+00:00"
    },
    "disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index. Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
    "bpi":{
        "USD":{
            "code":"USD",
            "symbol":"$",
            "rate":"126.5235",
            "description":"United States Dollar",
            "rate_float":126.5235
        },
        "GBP":{
            "code":"GBP",
            "symbol":"£",
            "rate":"79.2495",
            "description":"British Pound Sterling",
            "rate_float":79.2495
        },
        "EUR":{
            "code":"EUR",
            "symbol":"€",
            "rate":"94.7398",
            "description":"Euro",
            "rate_float":94.7398
        }
    }
}
*/
