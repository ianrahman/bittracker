The Task:

Create an iPhone app with a companion Watch app that will fetch the current exchange rate of Bitcoin/EUR (suggested resources are provided below, however you may decide to use any other) and display the data in a simple table view (on each the phone and the watch).

Must have:

    * Fetch the historical data for the last 2 weeks up to today (daily data, 1 value per day).
    * Show the data in a simple table view on the phone and on the watch (no need for fancy UI/UX here, just a plain simple UITableView will do).
    * The project must be done using Swift.

Nice to have (optional):

    * Continuously update the current price for today.
    * Cache the latest data to be shown upon app start while fetching new values.

The extra mile (purely optional):

    * Create the Today Extension with the most recent Bitcoin value.

Note:

    * A decent test coverage is expected.

Suggested Resources

http://www.coindesk.com/api/
